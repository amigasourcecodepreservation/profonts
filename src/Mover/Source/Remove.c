/*
 *	System Mover
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Remove routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <TypeDefs.h>

#include <Toolbox/Utility.h>
#include <Toolbox/List.h>

#include "Mover.h"

/*
 *	External variables
 */

extern WindowPtr	window;

extern BPTR	fromLock;

extern ListHeadPtr	windList, fileList;

extern WORD		moveType;
extern UBYTE	*moveDir;

/*
 *	Local variables and definitions
 */

typedef struct _FCList {
	struct _FCList		*Next;
	struct FontContents	FC;
} FCList, *FCListPtr;

#define FCH_SIZE	(sizeof(struct FontContentsHeader))
#define FC_SIZE		(sizeof(struct FontContents))
#define FCL_SIZE	(sizeof(FCList))

/*
 *	Remove specified font
 *	Return success status
 */

static BOOL RemoveFont(fileName)
UBYTE *fileName;
{
	register WORD num;
	register BOOL success;
	register BPTR dir, file;
	UBYTE name[64];
	register FCListPtr firstFCL, currFCL, newFCL;
	struct FontContentsHeader fch;
	struct FontContents fc;

	success = FALSE;
	firstFCL = currFCL = NULL;
	file = NULL;
/*
	Switch to source fonts directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock("fonts", ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Remove font
*/
	if (!DeleteFile(fileName))
		goto Exit;
	success = TRUE;				/* Success even if ".font" file is incorrect */
/*
	Remove entry for font in ".font" file
	First build list of data files to remain in ".font" file
*/
	BuildName(fileName, ".font", name);
	if ((file = Open(name, MODE_OLDFILE)) == NULL)
		goto Exit;
	if (Read(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE)
		goto Exit;
	num = 0;
	while (Read(file, (BYTE *) &fc, FC_SIZE) == FC_SIZE){
		if (CompString(fc.fc_FileName, fileName, (WORD) strlen(fc.fc_FileName),
					   (WORD) strlen(fileName), FALSE) == 0)
			continue;
		num++;
		if ((newFCL = (FCListPtr) AllocMem(FCL_SIZE, 0)) == NULL)
			goto Exit;
		memcpy(&(newFCL->FC), &fc, FC_SIZE);
		newFCL->Next = NULL;
		if (firstFCL == NULL)
			firstFCL = newFCL;
		else
			currFCL->Next = newFCL;
		currFCL = newFCL;
	}
	Close(file);
	file = NULL;
/*
	Write new ".font" file
*/
	if (!DeleteFile(name))
		goto Exit;
	if (num == 0) {								/* No fonts in this family */
		BuildName(fileName, ".metric", name);
		(void) DeleteFile(name);				/* Delete metric file */
		BuildName(fileName, "", name);
		(void) DeleteFile(name);				/* Delete family directory */
	}
	else {
		if ((file = Open(name, MODE_NEWFILE)) == NULL)
			goto Exit;
		fch.fch_NumEntries = num;
		if (Write(file, (BYTE *) &fch, FCH_SIZE) != FCH_SIZE)
			goto Exit;
		currFCL = firstFCL;
		while (num--) {
			if (Write(file, (BYTE *) &(currFCL->FC), FC_SIZE) != FC_SIZE)
				goto Exit;
			currFCL = currFCL->Next;
		}
	}
/*
	All done
*/
Exit:
	while (firstFCL) {
		newFCL = firstFCL->Next;
		FreeMem((BYTE *) firstFCL, FCL_SIZE);
		firstFCL = newFCL;
	}
	if (file)
		Close(file);
	return (success);
}

/*
 *	Remove specified file
 *	Return success status
 */

static BOOL RemoveFile(fileName)
UBYTE *fileName;
{
	register BOOL success;
	register BPTR dir;

	success = FALSE;
/*
	Switch to source directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(moveDir, ACCESS_READ)) == NULL)
		goto Exit;
	SetDir(dir);
/*
	Remove file
*/
	if (!DeleteFile(fileName))
		goto Exit;
/*
	All done
*/
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Handle "Remove" button
 */

void DoRemove()
{
	register UBYTE *text, *fileName;
	register BOOL success;
	register WORD i;

	if (fromLock == NULL || windList == NULL || fileList == NULL)
		return;
/*
	Remove each item one by one
*/
	SetStdPointer(window, POINTER_WAIT);
	DisableInput();
	OffButtons();
	EraseInfo();
	for (i = 0; i < NumListItems(windList); i++) {
		text = GetListItem(windList, (LONG) i);
		if (*text == '*') {
			fileName = GetListItem(fileList, (LONG) i);
			switch (moveType) {
			case FONTS_ITEM:
				success = RemoveFont(fileName);
				break;
			default:
				success = RemoveFile(fileName);
				break;
			}
			if (!success) {
				SysBeep(5);
				DisplayBeep(window->WScreen);
				break;
			}
			RemoveListItem(windList, (LONG) i);
			RemoveListItem(fileList, (LONG) i);
			i--;
		}
	}
	SetStdPointer(window, POINTER_ARROW);
	DeSelectAll();
	DrawWindList(0);
	EnableInput();
	SetButtons();
}
