/*
 *	System Mover
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Info routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <exec/resident.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <TypeDefs.h>

#include <Toolbox/Utility.h>
#include <Toolbox/List.h>

#include "Mover.h"

/*
 *	External variables
 */

extern WindowPtr	window;

extern TextFontPtr	topaz8Font;

extern BPTR	fromLock;

extern ListHeadPtr	fileList;

extern WORD		moveType;
extern UBYTE	*moveDir;

/*
 *	Local variables and definitions
 */

#define SAMPLE_LEN	44

static TextChar	sampleText[] = "The quick brown fox jumps over the lazy dog.";

struct FontData {
	ULONG					NextSegment, ReturnCode;
	struct DiskFontHeader	DFH;
};

struct LibData {
	ULONG	NextSegment;
	UWORD	Data[1];			/* First word in segment data */
};

/*
 *	Show font info
 */

static void ShowFontInfo(fileName)
TextPtr fileName;
{
	register WORD vPos;
	register BPTR fontSeg;
	register struct FontData *fontData;
	register RastPtr rPort = window->RPort;

/*
	Load the font file
*/
	if ((fontSeg = LoadSeg(fileName)) == NULL)
		goto Exit;
	fontData = (struct FontData *) BADDR(fontSeg);
/*
	Draw sampleText in this font
*/
	SetAPen(rPort, COLOR_BLACK);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, &(fontData->DFH.dfh_TF));
	vPos = 20;
	if (vPos < fontData->DFH.dfh_TF.tf_Baseline)
		vPos = fontData->DFH.dfh_TF.tf_Baseline;
	vPos += window->BorderTop + (INFO_TOP + INFO_MARG);
	if (vPos > window->Height - window->BorderBottom - INFO_MARG)
		vPos = window->Height - window->BorderBottom - INFO_MARG;
	Move(rPort, window->BorderLeft + INFO_MARG, vPos);
	Text(rPort, sampleText, SAMPLE_LEN);
/*
	Clean up
*/
	UnLoadSeg(fontSeg);
Exit:
	SetFont(rPort, topaz8Font);
}

/*
 *	Show general file info
 */

static void ShowGenInfo(fileName)
TextPtr fileName;
{
	register WORD i, len, frac;
	register BPTR lock;
	register struct FileInfoBlock *fib;
	register RastPtr rPort = window->RPort;
	UBYTE buff[100];

	fib = NULL;
	lock = NULL;
/*
	Get information on the file
*/
	if ((fib = (struct FileInfoBlock *) AllocMem(sizeof(struct FileInfoBlock), 0))
		== NULL)
		return;
	if ((lock = Lock(fileName, ACCESS_READ)) == NULL || !Examine(lock, fib))
		goto Exit;
/*
	Display info
*/
	SetAPen(rPort, COLOR_BLACK);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, topaz8Font);
	Move(rPort, window->BorderLeft + INFO_MARG,
				window->BorderTop + (INFO_TOP + INFO_MARG + 10));
	Text(rPort, "Size: ", 6);
	NumToString(fib->fib_Size, buff);
	len = strlen(buff);
	if ((frac = len % 3) != 0)
		Text(rPort, buff, frac);
	for (i = frac; i < len; i += 3) {
		if (frac || i > frac)
			Text(rPort, ",", 1);
		Text(rPort, buff + i, 3);
	}
	Text(rPort, " bytes", 6);
	Move(rPort, window->BorderLeft + INFO_MARG,
				window->BorderTop + (INFO_TOP + INFO_MARG + 25));
	Text(rPort, "Created: ", 9);
	DateString(&(fib->fib_Date), DATE_ABBR, buff);
	Text(rPort, buff, strlen(buff));
	Text(rPort, ", ", 2);
	TimeString(&(fib->fib_Date), FALSE, TRUE, buff);
	Text(rPort, buff, strlen(buff));
/*
	Clean up
*/
Exit:
	if (lock)
		UnLock(lock);
	if (fib)
		FreeMem((BYTE *) fib, sizeof(struct FileInfoBlock));
}

/*
 *	Find string length up to CR, LF, or NULL
 */

static WORD StrLength(s)
register TextPtr s;
{
	register WORD len;

	len = 0;
	while (*s && *s != '\r' && *s != '\n') {
		s++;
		len++;
	}
	return (len);
}

/*
 *	Show library and device info
 */

static void ShowLibsInfo(fileName)
TextPtr fileName;
{
	register WORD i, size;
	register BPTR libSeg;
	LONG *longPtr;
	register struct LibData *libData;
	register struct Resident *res;
	register RastPtr rPort = window->RPort;
	UBYTE buff[10];

/*
	Load the library or device file
*/
	if ((libSeg = LoadSeg(fileName)) == NULL)
		return;
	libData = (struct LibData *) BADDR(libSeg);
/*
	Find start of resident struct
*/
	longPtr = (LONG *) libData;
	size = (*(longPtr - 1) - sizeof(LONG) - sizeof(struct Resident))/sizeof(WORD);
	for (i = 0; i <= size; i++) {
		if (libData->Data[i] == RTC_MATCHWORD)
			break;
	}
	if (libData->Data[i] != RTC_MATCHWORD) {
		UnLoadSeg(libSeg);
		ShowGenInfo(fileName);
		return;
	}
	res = (struct Resident *) &(libData->Data[i]);
/*
	Display info
*/
	SetAPen(rPort, COLOR_BLACK);
	SetDrMd(rPort, JAM1);
	SetFont(rPort, topaz8Font);
	Move(rPort, window->BorderLeft + INFO_MARG,
				window->BorderTop + (INFO_TOP + INFO_MARG + 10));
	Text(rPort, "ID: ", 4);
	Text(rPort, res->rt_IdString, StrLength(res->rt_IdString));
	Move(rPort, window->BorderLeft + INFO_MARG,
				window->BorderTop + (INFO_TOP + INFO_MARG + 25));
	Text(rPort, "Version: ", 9);
	NumToString((LONG) res->rt_Version, buff);
	Text(rPort, buff, strlen(buff));
/*
	Clean up
*/
	UnLoadSeg(libSeg);
}

/*
 *	Erase info area of window
 */

void EraseInfo()
{
	register WORD minX, minY, maxX, maxY;
	register RastPtr rPort = window->RPort;

	minX = window->BorderLeft + INFO_MARG;
	maxX = window->Width - window->BorderRight - INFO_MARG - 1;
	minY = window->BorderTop + INFO_TOP + INFO_MARG;
	maxY = window->Height - window->BorderBottom - INFO_MARG - 1;
	SetAPen(rPort, COLOR_WHITE);
	SetDrMd(rPort, JAM1);
	RectFill(rPort, minX, minY, maxX, maxY);
}

/*
 *	Show info for specified item in list
 */

void ShowInfo(item)
WORD item;
{
	register TextPtr fileName;
	register BPTR dir;
	register RegionPtr clipRegion, oldRegion;
	struct Rectangle rect;

/*
	Install clip region in info area
*/
	if ((clipRegion = NewRegion()) != NULL) {
		rect.MinX = window->BorderLeft + INFO_MARG;
		rect.MaxX = window->Width - window->BorderRight - INFO_MARG - 1;
		rect.MinY = window->BorderTop + INFO_TOP + INFO_MARG;
		rect.MaxY = window->Height - window->BorderBottom - INFO_MARG - 1;
		OrRectRegion(clipRegion, &rect);
		oldRegion = InstallClipRegion(window->WLayer, clipRegion);
		if (oldRegion)
			DisposeRegion(oldRegion);
	}
/*
	Switch to appropriate source directory
*/
	dir = DupLock(fromLock);
	SetDir(dir);
	if ((dir = Lock(moveDir, ACCESS_READ)) == NULL)
		return;
	SetDir(dir);
	fileName = GetListItem(fileList, (LONG) item);
/*
	Show appropriate type of info
*/
	switch (moveType) {
	case FONTS_ITEM:
		ShowFontInfo(fileName);
		break;
	case LIBRARIES_ITEM:
	case DEVICES_ITEM:
		ShowLibsInfo(fileName);
		break;
	case PRINTERS_ITEM:
	case KEYMAPS_ITEM:
	case HANDLERS_ITEM:
	case CLI_ITEM:
	case SCRIPTS_ITEM:
		ShowGenInfo(fileName);
		break;
	}
/*
	Clean up
*/
	clipRegion = InstallClipRegion(window->WLayer, NULL);
	if (clipRegion)
		DisposeRegion(clipRegion);
}
