/*
 *	System Mover
 *	Copyright (c) 1987 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <Toolbox/List.h>

#include "Mover.h"

struct IntuitionBase	*IntuitionBase;
struct GfxBase		*GfxBase;
struct LibBase		*LayersBase;
struct LibBase		*IconBase;
struct LibBase		*DiskfontBase;

struct Window	*window;
struct MsgPort	*mainMsgPort;

BOOL	quitFlag;	/* Inited to FALSE */

struct TextFont	*topaz8Font;

BOOL	titleChanged;	/* Screen title was changed by Error() (Inited to FALSE) */

BPTR	fromLock, toLock;	/* From/To locks (Inited to NULL) */

ListHead	*windList, *fileList;	/* Inited to NULL */

WORD	moveType = FONTS_ITEM;	/* Type of item to move */
UBYTE	*moveDir = "fonts";
