/*
 *	System Mover
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Definitions
 */

#define COLOR_BLUE	0
#define COLOR_WHITE	1
#define COLOR_BLACK	2
#define COLOR_RED	3

/*
 *	Menu definitions
 */

#define DISK_MENU		0
#define FROMDISK_ITEM	0
#define TODISK_ITEM		1
#define QUIT_ITEM		3

#define MOVE_MENU		1
#define FONTS_ITEM		0
#define PRINTERS_ITEM	1
#define KEYMAPS_ITEM	2
#define LIBRARIES_ITEM	4
#define DEVICES_ITEM	5
#define HANDLERS_ITEM	6
#define CLI_ITEM		8
#define SCRIPTS_ITEM	9

/*
 *	Gadget definitions
 */

#define COPY_BUTTON		0
#define REMOVE_BUTTON	1
#define LIST_UP			2
#define LIST_DOWN		3
#define LIST_SCROLL		4

/*
 *	Misc definitions
 */

#define NUM_ENTRIES	7

#define LIST_LEFT	20
#define LIST_TOP	28
#define LIST_WIDTH	177		/* Allows 20 char file names */
#define LIST_HEIGHT	(NUM_ENTRIES*8)

#define INFO_MARG	5
#define INFO_TOP	(LIST_TOP + LIST_HEIGHT + 6)	/* Location of dotted line */
#define INFO_HEIGHT	40

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
