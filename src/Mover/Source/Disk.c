/*
 *	System Mover
 *	Copyright (c) 1988 New Horizons Software, Inc.
 *
 *	Disk menu routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Menu.h>
#include <Toolbox/List.h>

#include "Mover.h"

/*
 *  External routines
 */

extern void	DrawDiskName(UBYTE *, WORD);

extern void	DisposeFileLists();
extern void	GetFileLists();

/*
 *  External variables
 */

extern struct Window	*window;

extern BOOL	quitFlag;

extern BPTR	fromLock, toLock;

extern WORD	moveType;

/*
 *  Local variables and definitions
 */

#define VOLUME(lock)	\
    ((struct DeviceList *) BADDR(((struct FileLock *) BADDR(lock))->fl_Volume))

#define NEXT_DEVICE(device)	((struct DeviceList *) BADDR(device->dl_Next))

static WORD	numDisks;			/* Number of disks in list */
static struct DeviceList	**diskList;	/* Pointer to array of pointers */

static ListHead	*diskNames;

/*
 *  Switch to specified directory
 */

void SetDir(lock)
BPTR lock;
{
    register BPTR oldLock;

    oldLock = CurrentDir(lock);
    UnLock(oldLock);
}

/*
 *  Free memory used by disk list
 *  Also remove and dispose of disk sub menus
 */

void DisposeDiskList()
{
    register WORD itemNum;
    register struct MenuItem *menuItem, *subItems;

/*
    Dispose of sub menus
*/
    itemNum = SHIFTMENU(DISK_MENU) | SHIFTITEM(FROMDISK_ITEM) | SHIFTSUB(NOSUB);
    menuItem = ItemAddress(window->MenuStrip, itemNum);
    subItems = menuItem->SubItem;
    InsertSubMenu(window, itemNum, NULL);
    if (subItems)
	DisposeMenuItems(subItems);
    itemNum = SHIFTMENU(DISK_MENU) | SHIFTITEM(TODISK_ITEM) | SHIFTSUB(NOSUB);
    menuItem = ItemAddress(window->MenuStrip, itemNum);
    subItems = menuItem->SubItem;
    InsertSubMenu(window, itemNum, NULL);
    if (subItems)
	DisposeMenuItems(subItems);
/*
    Dispose of disk name and volume pointer lists
*/
    DisposeList(diskNames);
    diskNames = NULL;
    if (numDisks && diskList) {
	FreeMem((BYTE *) diskList, numDisks*sizeof(struct DeviceList *));
	numDisks = 0;
	diskList = NULL;
    }
}

/*
 *  Set disk menu
 *  Check sub items for currently selected disks
 *  (Remember that names are added to list in reverse order of diskList array)
 */

void SetDiskMenu()
{
    register WORD i;
    register struct DeviceList *volume;

    volume = (fromLock) ? VOLUME(fromLock) : NULL;
    for (i = 0; i < numDisks; i++)
	CheckMenu(window,
		  (UWORD)(SHIFTMENU(DISK_MENU)|SHIFTITEM(FROMDISK_ITEM)|SHIFTSUB(i)),
		  (BOOL) (volume == diskList[numDisks - i - 1]));
    volume = (toLock) ? VOLUME(toLock) : NULL;
    for (i = 0; i < numDisks; i++)
	CheckMenu(window,
		  (UWORD)(SHIFTMENU(DISK_MENU)|SHIFTITEM(TODISK_ITEM)|SHIFTSUB(i)),
		  (BOOL) (volume == diskList[numDisks - i - 1]));
}

/*
 *  Handle disk inserted event
 *  Set up sub menus for "To" and "From" disk menu items
 *  Build list of disks
 */

void DoDiskInserted()
{
    register WORD i, num;
    register struct DeviceList *volume;
    UBYTE *name;
    MenuItemTemplate *template;
    struct MenuItem *menuItems;
    struct RootNode *rootNode;
    struct DosInfo *dosInfo;
    struct DeviceList *list[100];

    DisposeDiskList();
    diskNames = CreateList();
    Forbid();
    rootNode = (struct RootNode *) DOSBase->dl_Root;
    dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
/*
    Scan list and build list of pointers to volume entries
*/
    num = 0;
    for (volume = (struct DeviceList *) BADDR(dosInfo->di_DevInfo); volume;
	 volume = NEXT_DEVICE(volume)) {
	if (volume->dl_Type == DLT_VOLUME && num < 100) {
	    list[num++] = volume;
	    name = (BYTE *) BADDR(volume->dl_Name);
	    AddListItem(diskNames, name + 1, (WORD) *name, FALSE);
	}
    }
    Permit();
/*
    Now create permanent disk list
*/
    diskList = (struct DeviceList **) AllocMem(num*sizeof(struct DeviceList *), 0);
    if (diskList) {
	memcpy(diskList, list, num*sizeof(struct DeviceList *));
	numDisks = num;
    }
/*
    Create sub menus
*/
    template = (MenuItemTemplate *)
	       AllocMem((numDisks + 1)*sizeof(MenuItemTemplate), MEMF_CLEAR);
    if (template == NULL)
	return;
    for (i = 0; i < numDisks; i++) {
	template[i].Type = MENU_TEXT_ITEM;
	template[i].Flags = MENU_ENABLED | MENU_CHECKABLE;
	template[i].MutualExclude = ~(1 << i);
	template[i].Info = (APTR) GetListItem(diskNames, (LONG) i);
    }
    template[numDisks].Type = MENU_NO_ITEM;
    if ((menuItems = GetMenuItems(template)) != NULL)
	InsertSubMenu(window, SHIFTMENU(DISK_MENU) | SHIFTITEM(FROMDISK_ITEM),
		      menuItems);
    if ((menuItems = GetMenuItems(template)) != NULL)
	InsertSubMenu(window, SHIFTMENU(DISK_MENU) | SHIFTITEM(TODISK_ITEM),
		      menuItems);
    FreeMem((BYTE *) template, (numDisks + 1)*sizeof(MenuItemTemplate));
    if (menuItems == NULL)
	return;
/*
    Now check the disks that are currently selected
*/
    SetDiskMenu();
}

/*
 *  Check disk list to see if it is still correct
 *  Return TRUE if correct
 */

BOOL CheckDiskList()
{
    register WORD num;
    register BOOL success;
    register struct DeviceList *volume;
    struct RootNode *rootNode;
    struct DosInfo *dosInfo;

    Forbid();
    rootNode = (struct RootNode *) DOSBase->dl_Root;
    dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
/*
    Scan list and build list of pointers to volume entries
*/
    num = 0;
    success = TRUE;
    for (volume = (struct DeviceList *) BADDR(dosInfo->di_DevInfo); success && volume;
	 volume = NEXT_DEVICE(volume)) {
	if (volume->dl_Type == DLT_VOLUME &&
	    (num >= numDisks || volume != diskList[num++]))
	    success = FALSE;
    }
    Permit();
    if (num < numDisks)
	success = FALSE;
    return (success);
}

/*
 *  Get lock on given disk (from sub menu)
 */

BPTR LockDisk(name)
UBYTE *name;
{
    UBYTE buff[35];

    strcpy(buff, name);
    strcat(buff, ":");
    return (Lock(buff, ACCESS_READ));
}

/*
 *  Free disk locks on from/to disks
 */

void UnLockDisks()
{
    if (fromLock) {
	UnLock(fromLock);
	fromLock = NULL;
    }
    if (toLock) {
	UnLock(toLock);
	toLock = NULL;
    }
}

/*
 *  Handle Disk menu
 */

void DoDiskMenu(item, sub)
register UWORD item, sub;
{
    register UBYTE *name;

    switch (item) {
    case FROMDISK_ITEM:
	DisableInput();
	OffButtons();
	EraseWindList();
	EraseInfo();
	DisposeFileLists();
	name = GetListItem(diskNames, (LONG) sub);
	if (fromLock)
	    UnLock(fromLock);
	fromLock = LockDisk(name);
	SetDiskMenu();
	if (fromLock)
	    DrawDiskName(name, 0);
	else
	    DrawDiskName("", 0);
	GetFileLists();
	DrawWindList(0);
	EnableInput();
	break;
    case TODISK_ITEM:
	name = GetListItem(diskNames, (LONG) sub);
	if (toLock)
	    UnLock(toLock);
	toLock = LockDisk(name);
	SetDiskMenu();
	if (toLock)
	    DrawDiskName(name, 1);
	else
	    DrawDiskName("", 1);
	SetButtons();
	break;
    case QUIT_ITEM:
	quitFlag = TRUE;
	break;
    }
}

/*
 *  Handle Move menu
 */

void DoMoveMenu(item)
register UWORD item;
{
    if (item == moveType)
	return;
    moveType = item;
    DisableInput();
    OffButtons();
    EraseWindList();
    EraseInfo();
    DisposeFileLists();
    GetFileLists();
    DrawWindList(0);
    EnableInput();
}
