/*
 *	System Mover
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Window, screen, and window gadget definitions
 *	(except for items that must be in chip memory)
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Gadget.h>

#include "Mover.h"

/*
 *	Window definitions
 */

struct NewWindow newWindow = {
	0, 0, LIST_WIDTH + 120 + 8, INFO_TOP + INFO_HEIGHT + 14, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP | CLOSEWINDOW | MENUPICK |
		INTUITICKS | DISKINSERTED | DISKREMOVED,
	WINDOWDEPTH | WINDOWCLOSE | WINDOWDRAG | SMART_REFRESH | ACTIVATE,
	NULL, NULL, "System Mover", NULL, NULL,
	0, 0, 0, 0,
	WBENCHSCREEN
};

/*
 *	Gadget template for document window
 */

GadgetTemplate windowGadgets[] = {
	{ LIST_WIDTH + 40, LIST_TOP, 60, 14, GADG_PUSH_BUTTON, "Copy" },
	{ LIST_WIDTH + 40, LIST_TOP + 20, 60, 14, GADG_PUSH_BUTTON, "Remove" },
	{ LIST_LEFT + LIST_WIDTH - IMAGE_ARROW_WIDTH,
		LIST_TOP + LIST_HEIGHT - 2*IMAGE_ARROW_HEIGHT,
		IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		GADG_ACTIVE_STDIMAGE, (Ptr) IMAGE_UP_ARROW },
	{ LIST_LEFT + LIST_WIDTH - IMAGE_ARROW_WIDTH,
		LIST_TOP + LIST_HEIGHT - IMAGE_ARROW_HEIGHT,
		IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		GADG_ACTIVE_STDIMAGE, (Ptr) IMAGE_DOWN_ARROW },
	{ LIST_LEFT + LIST_WIDTH - IMAGE_ARROW_WIDTH,
		LIST_TOP,
		IMAGE_ARROW_WIDTH, LIST_HEIGHT - 2*IMAGE_ARROW_HEIGHT,
		GADG_PROP_VERT, NULL },
	{ 0, 0, 0, 0, GADG_ITEM_NONE, NULL }
};
