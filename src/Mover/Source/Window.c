/*
 *	System Mover
 *	Copyright 1988 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>

#include "Mover.h"

/*
 *  External variables
 */

extern struct NewWindow		newWindow;

extern GadgetTemplate	windowGadgets[];
extern MenuTemplate	newMenus[];

extern UBYTE	screenTitle[];

extern struct Window	*window;

extern struct TextFont	*topaz8Font;

/*
 *  Local routines
 */

extern void	RemoveWindow(void);

/*
 *  Local variables and definitions
 */

#define MAX_CHARS	((LIST_WIDTH + 100 - 6*8)/8)

static WORD listBorder[] = {
	LIST_LEFT + LIST_WIDTH,				LIST_TOP - 1,
	LIST_LEFT + LIST_WIDTH,				LIST_TOP + LIST_HEIGHT,
	LIST_LEFT - 1,					LIST_TOP + LIST_HEIGHT,
	LIST_LEFT - 1,					LIST_TOP - 1,
	LIST_LEFT - 2,					LIST_TOP - 1,
	LIST_LEFT - 2,					LIST_TOP + LIST_HEIGHT,
	LIST_LEFT + LIST_WIDTH + 1,			LIST_TOP + LIST_HEIGHT,
	LIST_LEFT + LIST_WIDTH + 1,			LIST_TOP - 1,
	LIST_LEFT + LIST_WIDTH - IMAGE_ARROW_WIDTH - 1,	LIST_TOP - 1,
	LIST_LEFT + LIST_WIDTH - IMAGE_ARROW_WIDTH - 1,	LIST_TOP + LIST_HEIGHT
};

/*
 *  Draw disk name in window
 *  Position of 0 means upper (from) location, 1 means lower (to) location
 */

void DrawDiskName(name, position)
UBYTE *name;
WORD position;
{
    register WORD len, minX, minY;
    register struct RastPort *rPort = window->RPort;

    minX = window->BorderLeft;
    minY = window->BorderTop;
    SetAPen(rPort, COLOR_BLACK);
    SetBPen(rPort, COLOR_WHITE);
    SetDrMd(rPort, JAM2);
    SetFont(rPort, topaz8Font);
    Move(rPort, minX + (20 + 6*8), minY + position*11 + (3 + 7));
    len = strlen(name);
    if (len > MAX_CHARS) {
	Text(rPort, name, MAX_CHARS - 3);
	Text(rPort, "...", 3);
    }
    else {
	Text(rPort, name, len);
	while (len++ < MAX_CHARS)
	    Text(rPort, " ", 1);
    }
}

/*
 *  Draw window contents (except contents of list boxes)
 */

void DrawWindow()
{
    register WORD i, minX, minY, maxX, maxY;
    register struct RastPort *rPort = window->RPort;

    minX = window->BorderLeft;
    maxX = window->Width - window->BorderRight - 1;
    minY = window->BorderTop;
    maxY = window->Height - window->BorderBottom - 1;
    SetAPen(rPort, COLOR_WHITE);
    SetBPen(rPort, COLOR_WHITE);
    SetDrMd(rPort, JAM2);
    BNDRYOFF(rPort);
    RectFill(rPort, minX, minY, maxX, maxY);
/*
    Draw borders
*/
    SetAPen(rPort, COLOR_BLUE);
    Move(rPort, minX + LIST_LEFT - 1, minY + LIST_TOP - 1);
    for (i = 0; i < 20; i += 2) {
	listBorder[i] += minX;
	listBorder[i + 1] += minY;
    }
    PolyDraw(rPort, 10, listBorder);
    for (i = 0; i < 20; i += 2) {
	listBorder[i] -= minX;
	listBorder[i + 1] -= minY;
    }
    SetDrPt(rPort, 0xAAAA);
    Move(rPort, minX + INFO_MARG, minY + INFO_TOP);
    Draw(rPort, maxX - INFO_MARG, minY + INFO_TOP);
    SetDrPt(rPort, 0xFFFF);
/*
    Draw misc text
*/
    SetFont(rPort, topaz8Font);
    Move(rPort, minX + 20, minY + (3 + 7));
    Text(rPort, "From:", 5);
    Move(rPort, minX + 20, minY + (14 + 7));
    Text(rPort, "To:", 3);
}

/*
 *  Create and draw main window
 */

void CreateWindow()
{
    register WORD i, minX, minY;
    register struct Gadget *gadgetList;
    register struct Menu *menuStrip;

/*
    Open window
*/
    if ((window = OpenWindow(&newWindow)) == NULL)
	return;
    SetWindowTitles(window, (UBYTE *) -1, screenTitle);
/*
    Get gadgets for window and draw window contents
*/
    DrawWindow();
    minX = window->BorderLeft;
    minY = window->BorderTop;
    for (i = 0; windowGadgets[i].Type != GADG_ITEM_NONE; i++) {
	windowGadgets[i].LeftEdge += minX;
	windowGadgets[i].TopEdge += minY;
    }
    gadgetList = GetGadgets(windowGadgets, FALSE);
    if (gadgetList == NULL) {
	RemoveWindow();
	return;
    }
    for (i = 0; windowGadgets[i].Type != GADG_ITEM_NONE; i++) {
	windowGadgets[i].LeftEdge -= minX;
	windowGadgets[i].TopEdge -= minY;
    }
    AddGList(window, gadgetList, 0, -1, NULL);
    RefreshGadgets(gadgetList, window, NULL);
/*
    Get menu
*/
    if ((menuStrip = GetMenuStrip(newMenus)) == NULL) {
	RemoveWindow();
	return;
    }
    InsertMenuStrip(window, menuStrip);
}

/*
 *  Remove main window
 */

void RemoveWindow()
{
    register struct Region *clipRegion;
    register struct Gadget *gadgetList;
    register struct Menu *menuStrip;

    if (window == NULL)
	return;
    menuStrip = window->MenuStrip;
    ClearMenuStrip(window);
    gadgetList = GadgetItem(window->FirstGadget, 0);	/* First user gadget */
    clipRegion = InstallClipRegion(window->WLayer, NULL);
    CloseWindow(window);
    if (clipRegion)
	DisposeRegion(clipRegion);
    if (gadgetList)
	DisposeGadgets(gadgetList);
    if (menuStrip)
	DisposeMenuStrip(menuStrip);
}
