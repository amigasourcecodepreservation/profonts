/*
 *	System Mover
 *	Copyright (c) 1988 New Horizons Software, Inc.
 *
 *	List routines
 *	The fileList is a list of actual filenames
 *	    (relative to the appropriate directory)
 *	The windList is a list for the scrolling display
 *	    The first char of windList entries is a ' ' for non-selected,
 *	    and '*' for selected entries
 *	    This first char is not displayed in the window
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/List.h>
#include <Toolbox/Utility.h>

#include "Mover.h"

/*
 *  External routines
 */

extern void	SetDir(BPTR);

extern void	OffButtons();
extern void	SetButtons();

/*
 *  External variables
 */

extern BPTR	fromLock;

extern ListHead	*windList, *fileList;

extern struct Window	*window;

extern WORD	moveType;
extern UBYTE	*moveDir;

/*
 *  Local variables and definitions
 */

#define IS_DIR(fib)	((fib)->fib_DirEntryType > 0)
#define IS_FILE(fib)	((fib)->fib_DirEntryType < 0)

#define MAX_CHARS	((LIST_WIDTH - IMAGE_ARROW_WIDTH - 1)/8)

/*
 *  Get lists of fonts
 */

static void GetFontLists()
{
    register WORD i, num, fontLen, sizeLen, maxChars;
    BPTR dir, dir1, dir2;
    register struct FileInfoBlock *fib1, *fib2;
    UBYTE *fontName, *sizeName, listName[64];

    moveDir = "fonts";
/*
    Go to fonts directory
*/
    dir = DupLock(fromLock);
    SetDir(dir);
    if ((dir = Lock(moveDir, ACCESS_READ)) == NULL)
	return;
    SetDir(dir);
/*
    Set up
*/
    dir1 = NULL;
    fib1 = fib2 = NULL;
    if ((dir1 = DupLock(dir)) == NULL)
	goto Exit;
    if ((fib1 = (struct FileInfoBlock *) AllocMem(sizeof(struct FileInfoBlock), 0))
	== NULL)
	goto Exit;
    if ((fib2 = (struct FileInfoBlock *) AllocMem(sizeof(struct FileInfoBlock), 0))
	== NULL)
	goto Exit;
    fontName = fib1->fib_FileName;
    sizeName = fib2->fib_FileName;
/*
    Scan fonts directory
*/
    if (!Examine(dir1, fib1) || IS_FILE(fib1))
	goto Exit;
    while (ExNext(dir1, fib1)) {
	if (IS_FILE(fib1))
	    continue;
	if ((dir2 = Lock(fontName, ACCESS_READ)) == NULL)
	    continue;
	if (!Examine(dir2, fib2)) {
	    UnLock(dir2);
	    continue;
	}
	fontLen = strlen(fontName);
/*
    Get list of actual font data files
*/
	while (ExNext(dir2, fib2)) {
	    if (IS_DIR(fib2))
		continue;
/*
    Add to windList first (this list should be sorted)
*/
	    sizeLen = strlen(sizeName);
	    maxChars = MAX_CHARS - sizeLen;
	    listName[0] = ' ';
	    if (fontLen <= maxChars) {
		memcpy(listName + 1, fontName, fontLen);
		for (i = fontLen; i < maxChars; i++)
		    listName[i + 1] = ' ';
	    }
	    else {
		memcpy(listName + 1, fontName, maxChars - 3);
		for (i = maxChars - 3; i < maxChars; i++)
		    listName[i + 1] = '.';
	    }
	    strcpy(listName + maxChars + 1, sizeName, sizeLen);
	    if ((num = AddListItem(windList, listName, MAX_CHARS + 1, TRUE)) == -1)
		break;
/*
    Now add to fileList at same location in list as windList
*/
	    strcpy(listName, fontName);
	    strcat(listName, "/");
	    strcat(listName, sizeName);
	    if (!InsertListItem(fileList, listName, (WORD) (fontLen + sizeLen + 1),
				(LONG) num)) {
		RemoveListItem(windList, (LONG) num);
		break;
	    }
	}
	UnLock(dir2);
    }
/*
    Clean up
*/
Exit:
    if (fib2)
	FreeMem((BYTE *) fib2, sizeof(struct FileInfoBlock));
    if (fib1)
	FreeMem((BYTE *) fib1, sizeof(struct FileInfoBlock));
    if (dir1)
	UnLock(dir1);
}

/*
 *  Get file and window lists for specified directory
 *  If fileExt is specified then only names with proper file extension will be added
 */

static void GetGenLists(dirName, fileExt)
UBYTE *dirName, *fileExt;
{
    register WORD i, num, nameLen, extLen;
    BPTR dir, dir1;
    register struct FileInfoBlock *fib1;
    UBYTE *fileName, listName[64];

    moveDir = dirName;
/*
    Go to specified directory
*/
    dir = DupLock(fromLock);
    SetDir(dir);
    if ((dir = Lock(dirName, ACCESS_READ)) == NULL)
	return;
    SetDir(dir);
/*
    Set up
*/
    dir1 = NULL;
    fib1 = NULL;
    if ((dir1 = DupLock(dir)) == NULL)
	goto Exit;
    if ((fib1 = (struct FileInfoBlock *) AllocMem(sizeof(struct FileInfoBlock), 0))
	== NULL)
	goto Exit;
    fileName = fib1->fib_FileName;
    extLen = (fileExt) ? strlen(fileExt) : 0;
/*
    Scan directory
*/
    if (!Examine(dir1, fib1) || IS_FILE(fib1))
	goto Exit;
    while (ExNext(dir1, fib1)) {
	if (IS_DIR(fib1))
	    continue;
	nameLen = strlen(fileName);
	if (extLen &&
	    (nameLen < extLen || CompString(fileName + nameLen - extLen, fileExt,
					    extLen, extLen, FALSE) != 0))
	    continue;
/*
    Add to windList first (this list should be sorted)
*/
	listName[0] = ' ';
	if (nameLen <= MAX_CHARS) {
	    memcpy(listName + 1, fileName, nameLen);
	    for (i = nameLen; i < MAX_CHARS; i++)
		listName[i + 1] = ' ';
	}
	else {
	    memcpy(listName + 1, fileName, MAX_CHARS - 3);
	    for (i = MAX_CHARS - 3; i < MAX_CHARS; i++)
		listName[i + 1] = '.';
	}
	listName[MAX_CHARS + 1] = '\0';
	if ((num = AddListItem(windList, listName, MAX_CHARS + 1, TRUE)) == -1)
	    break;
/*
    Now add to fileList at same location in list as windList
*/
	if (!InsertListItem(fileList, fileName, (WORD) nameLen, (LONG) num)) {
	    RemoveListItem(windList, (LONG) num);
	    break;
	}
    }
/*
    Clean up
*/
Exit:
    if (fib1)
	FreeMem((BYTE *) fib1, sizeof(struct FileInfoBlock));
    if (dir1)
	UnLock(dir1);
}

/*
 *  Dispose of file and window lists
 */

void DisposeFileLists()
{
    if (windList)
	DisposeList(windList);
    if (fileList)
	DisposeList(fileList);
    windList = fileList = NULL;
}

/*
 *  Get file and window lists
 */

void GetFileLists()
{
/*
    Create empty list
*/
    if (windList)
	DisposeListItems(windList);
    else
	windList = CreateList();
    if (fileList)
	DisposeListItems(fileList);
    else
	fileList = CreateList();
    if (windList == NULL || fileList == NULL || fromLock == NULL)
	return;
/*
    Get appropriate lists and display
*/
    SetStdPointer(window, POINTER_WAIT);
    OffButtons();
    switch (moveType) {
    case FONTS_ITEM:
	GetFontLists();
	break;
    case PRINTERS_ITEM:
	GetGenLists("devs/printers", NULL);
	break;
    case KEYMAPS_ITEM:
	GetGenLists("devs/keymaps", NULL);
	break;
    case LIBRARIES_ITEM:
	GetGenLists("libs", ".library");
	break;
    case DEVICES_ITEM:
	GetGenLists("devs", ".device");
	break;
    case HANDLERS_ITEM:
	GetGenLists("l", NULL);
	break;
    case CLI_ITEM:
	GetGenLists("c", NULL);
	break;
    case SCRIPTS_ITEM:
	GetGenLists("s", NULL);
	break;
    }
    SetButtons();
    SetStdPointer(window, POINTER_ARROW);
}
