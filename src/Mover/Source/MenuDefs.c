/*
 *	System Mover
 *	Copyright (c) 1987 New Horizons Software, Inc.
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Menu.h>

/*
 *  Disk menu
 */

static MenuItemTemplate diskItems[] = {
    { MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, (APTR) "From Disk", NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, (APTR) "To Disk", NULL },
    { MENU_TEXT_ITEM,            0,   0, 0, 0, NULL, NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, (APTR) "Quit", NULL },
    { MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *  Type menu
 */

static MenuItemTemplate moveItems[] = {
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x376,
	(APTR) "Fonts", NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x375,
	(APTR) "Printers", NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x373,
	(APTR) "Key Maps", NULL },
    { MENU_TEXT_ITEM,            0, 0, 0, 0, NULL, NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x367,
	(APTR) "Libraries", NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x357,
	(APTR) "Devices", NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x337,
	(APTR) "Handlers", NULL },
    { MENU_TEXT_ITEM,            0, 0, 0, 0, NULL, NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x277,
	(APTR) "CLI Commands", NULL },
    { MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x177,
	(APTR) "CLI Scripts", NULL },
    { MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *  Menu strip template
 */

MenuTemplate newMenus[] = {
    { " Disk ", &diskItems[0] },
    { " Move ", &moveItems[0] },
    { NULL, NULL }
};
