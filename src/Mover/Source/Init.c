/*
 *	System Mover
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <graphics/gfxbase.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Utility.h>

/*
 *	External routines
 */

extern void ShutDown();

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;
extern struct GfxBase		*GfxBase;
extern struct LayersBase	*LayersBase;
extern struct Library		*IconBase;

extern struct NewWindow	newWindow;
extern struct Window	*window;
extern struct MsgPort	*mainMsgPort;

extern struct TextFont	*topaz8Font;

/*
 *	Local variables and definitions
 */

static BPTR	startupDir;

static struct TextAttr topaz8Attr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT
};

/*
 *	Abort initialization
 *	If message then display error message in workbench screen
 */

static void InitError(text)
UBYTE *text;
{
	struct IntuiText bodyText, negText;

	if (text) {
		bodyText.FrontPen  = negText.FrontPen  = AUTOFRONTPEN;
		bodyText.BackPen   = negText.BackPen   = AUTOBACKPEN;
		bodyText.DrawMode  = negText.DrawMode  = AUTODRAWMODE;
		bodyText.LeftEdge  = negText.LeftEdge  = AUTOLEFTEDGE;
		bodyText.TopEdge   = negText.TopEdge   = AUTOTOPEDGE;
		bodyText.ITextFont = negText.ITextFont = AUTOITEXTFONT;
		bodyText.NextText  = negText.NextText  = AUTONEXTTEXT;
		bodyText.IText = text;
		negText.IText = "Cancel";
		DisplayBeep(NULL);
		SysBeep(5);
		AutoRequest(NULL, &bodyText, NULL, &negText, 0L, 0L, 250, 50);
	}
	ShutDown();
	_exit(RETURN_FAIL);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open window
 */

void Init()
{
	register BPTR lock;
	register struct Process *process;
	struct Screen wbScreen;

	process = (struct Process *) FindTask(NULL);
/*
	Open needed libraries
*/
	IntuitionBase = (struct IntuitionBase *)
				OpenLibrary("intuition.library", 33);
	GfxBase = (struct GfxBase *)
				OpenLibrary("graphics.library", 33);
	LayersBase = (struct LayersBase *)
				OpenLibrary("layers.library", 33);
	if (IntuitionBase == NULL || GfxBase == NULL || LayersBase == NULL)
		InitError(NULL);
/*
	Find dimensions of workbench screen so we can center our window in it
*/
	if (!GetScreenData((BYTE *) &wbScreen, sizeof(struct Screen), WBENCHSCREEN,
						NULL))
		InitError("Can't Get Screen Info");
	newWindow.LeftEdge = (wbScreen.Width - newWindow.Width)/2;
	newWindow.TopEdge  = (wbScreen.Height - newWindow.Height)/2;
/*
	Open main window
*/
	topaz8Font = OpenFont(&topaz8Attr);				/* This can't fail */
	CreateWindow();
	if (window == NULL)
		InitError("Can't Open Window");
	mainMsgPort = window->UserPort;
/*
	Complete initialization
*/
	SetStdPointer(window, POINTER_ARROW);
	lock = DupLock(process->pr_CurrentDir);
	startupDir = CurrentDir(lock);				/* Save actual startup lock */
}

/*
 *	Shut down program
 *	Close window and all openned libraries
 */

void ShutDown()
{
	register BPTR lock;

	if (window) {
		lock = CurrentDir(startupDir);				/* In case running from CLI */
		UnLock(lock);
		CloseFont(topaz8Font);
		RemoveWindow();
	}
	if (LayersBase)
		CloseLibrary((struct Library *) LayersBase);
	if (GfxBase)
		CloseLibrary((struct Library *) GfxBase);
	if (IntuitionBase) {
		OpenWorkBench();
		CloseLibrary((struct Library *) IntuitionBase);
	}
}

/*
 *	Stub this out
 */

void MemCleanup()
{
}
