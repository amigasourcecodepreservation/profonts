/*
 *	System Mover
 *	Copyright (c) 1988 New Horizons Software, Inc.
 *
 *	_main routine, slightly modified from Lattice source
 */

#include <exec/types.h>
#include <workbench/startup.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <stdio.h>
#include <fcntl.h>
#include <ios1.h>
#include <string.h>

#ifndef TINY
extern struct UFB _ufbs[];
extern int _fmode, _iomode;
extern int (*_ONBREAK)();
extern int CXBRK();

#define MAXWINDOW 40
extern struct WBStartup *WBenchMsg;
static char window[MAXWINDOW + 25] = "con:10/10/320/80/Debug - ";
static struct Process *process;
static struct FileHandle *handle;
#endif

/*
 * Name		_main - process command line, open files, and call "main"
 *
 * Synopsis	_main(line);
 *		char *line;	ptr to command line that caused execution
 *
 */

void _main(line)
char *line;
{
#ifndef TINY
    register int x;
#endif

/*
    Open standard files
*/
#ifndef TINY
    if (*line == '\n' || *line == '\0') {	/* running under workbench */
	strncat(window, WBenchMsg->sm_ArgList->wa_Name, MAXWINDOW);
	_ufbs[0].ufbfh = Open(window, MODE_NEWFILE);
	_ufbs[1].ufbfh = _ufbs[0].ufbfh;
	_ufbs[1].ufbflg = UFB_NC;
	_ufbs[2].ufbfh = _ufbs[0].ufbfh;
	_ufbs[2].ufbflg = UFB_NC;
	handle = (struct FileHandle *) BADDR(_ufbs[0].ufbfh);
	process = (struct Process *) FindTask(NULL);
	process->pr_ConsoleTask = (APTR) handle->fh_Type;
	x = 0;
    }
    else {				/* running under CLI */
	_ufbs[0].ufbfh = Input();
	_ufbs[1].ufbfh = Output();
	_ufbs[2].ufbfh = Open("*", MODE_OLDFILE);
	x = UFB_NC;			/* do not close CLI defaults */
    }
    _ufbs[0].ufbflg |= UFB_RA | O_RAW | x;
    _ufbs[1].ufbflg |= UFB_WA | O_RAW | x;
    _ufbs[2].ufbflg |= UFB_RA | UFB_WA | O_RAW;
    x = (_fmode) ? 0 : _IOXLAT;
    stdin->_file  = 0;
    stdin->_flag  = _IOREAD | x;
    stdout->_file = 1;
    stdout->_flag = _IOWRT | x;
    stderr->_file = 2;
    stderr->_flag = _IORW | x;
    _ONBREAK = CXBRK;
#endif
/*
    Call user's main program
*/
    main();				/* call main function */
#ifndef TINY
    exit(0);
#else
    _exit(0);
#endif
}
