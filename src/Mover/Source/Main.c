/*
 *	System Mover
 *	Copyright (c) 1988 New Horizons Software, Inc.
 *
 *	Main Program
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Utility.h>

#include "Mover.h"

/*
 *  External routines
 */

extern void	DoDiskInserted();
extern void	DisposeDiskList();
extern BOOL	CheckDiskList();
extern void	UnLockDisks();
extern void	DoDiskMenu(UWORD, UWORD);

extern void	DisposeFileLists();
extern void	GetFileLists();

extern void	OffButtons();
extern void	DoGadgetDown(struct Gadget *);
extern void	DoGadgetUp(struct Gadget *);

/*
 *  External variables
 */

extern struct Window	*window;
extern struct MsgPort	*mainMsgPort;

extern BOOL	quitFlag;

/*
 *  Local routines
 */

extern void	DoIntuiMessage(struct IntuiMessage *);
extern void	DoMenu(UWORD, UWORD);

/*
 *  Local variables and definitions
 */

/*
 *  Main program
 */

void main()
{
    register struct IntuiMessage *intuiMsg;

/*
    Initialize
*/
    Init();
    OffButtons();
    DoDiskInserted();		/* Get initial disk list */
/*
    Get and respond to messages
*/
    do {
/*
    Process intuition messages
*/
	while (intuiMsg = (struct IntuiMessage *) GetMsg(mainMsgPort))
	    DoIntuiMessage(intuiMsg);
/*
    If not quitting then wait for messages
*/
	if (!quitFlag)
	    Wait(1 << mainMsgPort->mp_SigBit);
    } while (!quitFlag);
/*
    Quitting
*/
    SetStdPointer(window, POINTER_WAIT);
    UnLockDisks();
    DisposeDiskList();
    DisposeFileLists();
    ShutDown();
}

/*
 *  Process intuition messages
 */

void DoIntuiMessage(intuiMsg)
register struct IntuiMessage *intuiMsg;
{
    register ULONG class;
    register UWORD code;
    register APTR  iAddress;
    register WORD mouseX, mouseY;
    ULONG seconds, micros;
    register UWORD qualifier;
    register struct Window *msgWindow;

    class = intuiMsg->Class;
    code = intuiMsg->Code;
    iAddress = intuiMsg->IAddress;
    mouseX = intuiMsg->MouseX;
    mouseY = intuiMsg->MouseY;
    seconds = intuiMsg->Seconds;
    micros = intuiMsg->Micros;
    if (class != INTUITICKS)
	qualifier = intuiMsg->Qualifier;
    msgWindow = intuiMsg->IDCMPWindow;
    ReplyMsg((struct Message *) intuiMsg);
    if (msgWindow != window)
	return;
/*
    Process the message
*/
    switch (class) {
    case MOUSEBUTTONS:
	if (code == SELECTDOWN)
	    DoMouseDown(qualifier, mouseX, mouseY);
	break;
    case GADGETDOWN:
	DoGadgetDown((struct Gadget *) iAddress);
	break;
    case GADGETUP:
	DoGadgetUp((struct Gadget *) iAddress);
	break;
    case CLOSEWINDOW:
	quitFlag = TRUE;
	break;
    case MENUPICK:
	DoMenu(code, qualifier);
	break;
    case DISKINSERTED:
    case DISKREMOVED:
    case INTUITICKS:		/* In case somebody removed a lock */
	if (!CheckDiskList())
	    DoDiskInserted();
	break;
    }
}

/*
 *  Handle menu events
 */

void DoMenu(menuNumber, qualifier)
register UWORD menuNumber, qualifier;
{
    register UWORD menu, item, sub;

    while (menuNumber != MENUNULL) {
	menu = MENUNUM(menuNumber);
	item = ITEMNUM(menuNumber);
	sub = SUBNUM(menuNumber);
	switch (menu) {
	case DISK_MENU:
	    DoDiskMenu(item, sub);
	    break;
	case MOVE_MENU:
	    DoMoveMenu(item);
	    break;
	}
	menuNumber = ItemAddress(window->MenuStrip, menuNumber)->NextSelect;
    }
}
