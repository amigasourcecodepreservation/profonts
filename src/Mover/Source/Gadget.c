/*
 *	System Mover
 *	Copyright (c) 1988 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/List.h>

#include "Mover.h"

/*
 * External variables
 */

extern struct Window	*window;
extern struct MsgPort	*mainMsgPort;

extern BPTR	fromLock, toLock;

extern ListHead	*windList, *fileList;

/*
 *  Local variables and definitions
 */

#define VOLUME(lock)	\
    ((struct DeviceList *) BADDR(((struct FileLock *) BADDR(lock))->fl_Volume))

static WORD	listOffset;

/*
 *  Disable all buttons
 */

void OffButtons()
{
    register struct Gadget *gadget;

    gadget = GadgetItem(window->FirstGadget, COPY_BUTTON);
    OffGList(gadget, window, NULL, 2);
}

/*
 *  Enable/disable buttons in window
 */

void SetButtons()
{
    register WORD i, num;
    register BOOL select;
    register struct Gadget *gadget;
    register struct Gadget *gadgList = window->FirstGadget;

/*
    See if any items are selected
*/
    select = FALSE;
    if (fromLock && windList) {
	num = NumListItems(windList);
	for (i = 0; i < num; i++) {
	    if (*(GetListItem(windList, (LONG) i)) == '*') {
		select = TRUE;
		break;
	    }
	}
    }
/*
    Enable/Disable "Copy" button
*/
    gadget = GadgetItem(gadgList, COPY_BUTTON);
    if (toLock && select && VOLUME(fromLock) != VOLUME(toLock))
	OnGList(gadget, window, NULL, 1);
    else
	OffGList(gadget, window, NULL, 1);
/*
    Enable/Disable "Remove" button
*/
    gadget = GadgetItem(gadgList, REMOVE_BUTTON);
    if (select)
	OnGList(gadget, window, NULL, 1);
    else
	OffGList(gadget, window, NULL, 1);
}

/*
 *  Disable menus and scroll gadgets
 */

void DisableInput()
{
    register struct Gadget *gadget;

    gadget = GadgetItem(window->FirstGadget, LIST_UP);
    OffGList(gadget, window, NULL, 3);
    OffMenu(window, SHIFTMENU(DISK_MENU) | SHIFTITEM(NOITEM));
    OffMenu(window, SHIFTMENU(MOVE_MENU) | SHIFTITEM(NOITEM));
}

/*
 *  Enable menus and scroll gadgets
 */

void EnableInput()
{
    register struct Gadget *gadget;

    gadget = GadgetItem(window->FirstGadget, LIST_UP);
    OnGList(gadget, window, NULL, 3);
    OnMenu(window, SHIFTMENU(DISK_MENU) | SHIFTITEM(NOITEM));
    OnMenu(window, SHIFTMENU(MOVE_MENU) | SHIFTITEM(NOITEM));
}

/*
 *  Adjust scroll bar
 */

void AdjustScrollBar()
{
    register WORD num;
    register LONG newPot, newBody;
    register struct PropInfo *propInfo;
    register struct Gadget *gadget, *gadgList;

    gadgList = window->FirstGadget;
    gadget = GadgetItem(gadgList, LIST_SCROLL);
    propInfo = (struct PropInfo *) gadget->SpecialInfo;
    num = NumListItems(windList);
    if (num > NUM_ENTRIES) {
	newPot = (0xFFFFL*listOffset)/(num - NUM_ENTRIES);
	newBody = 0xFFFFL*NUM_ENTRIES/num;
    }
    else {
	newPot = 0xFFFFL;
	newBody = 0xFFFFL;
    }
    if (propInfo->VertPot != newPot || propInfo->VertBody != newBody)
	NewModifyProp(gadget, window, NULL, AUTOKNOB | FREEVERT,
		      0, newPot, 0xFFFFL, newBody, 1);
}

/*
 *  Erase window list
 */

void EraseWindList()
{
    register WORD minX, minY, maxX, maxY;
    register struct RastPort *rPort = window->RPort;

    minX = window->BorderLeft + LIST_LEFT;
    maxX = minX + (LIST_WIDTH - IMAGE_ARROW_WIDTH - 2);
    minY = window->BorderTop + LIST_TOP;
    maxY = minY + (LIST_HEIGHT - 1);
    SetAPen(rPort, COLOR_WHITE);
    SetBPen(rPort, COLOR_WHITE);
    SetDrMd(rPort, JAM2);
    RectFill(rPort, minX, minY, maxX, maxY);
}

/*
 *  Draw windList starting at appropriate offset
 */

void DrawWindList(offset)
WORD offset;
{
    register UBYTE *text;
    register WORD i, num, x, y;
    register struct RastPort *rPort = window->RPort;

    num = NumListItems(windList);
    if (offset > num - NUM_ENTRIES)
	offset = num - NUM_ENTRIES;
    if (offset < 0)
	offset = 0;
    x = window->BorderLeft + LIST_LEFT;
    y = window->BorderTop + LIST_TOP;
    for (i = 0; i < NUM_ENTRIES; i++) {
	text = GetListItem(windList, (LONG) (i + offset));
	if (text == NULL)
	    break;
	Move(rPort, x, y + 6);
	SetAPen(rPort, COLOR_BLUE);
	SetBPen(rPort, COLOR_WHITE);
	SetDrMd(rPort, JAM2);
	Text(rPort, text + 1, strlen(text) - 1);
	if (*text == '*') {
	    SetAPen(rPort, 0x03);
	    SetDrMd(rPort, COMPLEMENT);
	    RectFill(rPort, x, y, x + (LIST_WIDTH - IMAGE_ARROW_WIDTH - 2), y + 7);
	}
	y += 8;
    }
    if (i < NUM_ENTRIES) {
	SetAPen(rPort, COLOR_WHITE);
	SetBPen(rPort, COLOR_WHITE);
	SetDrMd(rPort, JAM2);
	RectFill(rPort, x, y, x + (LIST_WIDTH - IMAGE_ARROW_WIDTH - 2),
		 y + LIST_HEIGHT - i*8 - 1);
    }
    listOffset = offset;
    AdjustScrollBar();
}

/*
 *  Adjust display to new scroll bar setting
 */

void AdjustToScrollBar()
{
    register WORD num;
    register LONG newOffset;
    register struct Gadget *gadget, *gadgList;
    register struct PropInfo *propInfo;

    gadgList = window->FirstGadget;
    gadget = GadgetItem(gadgList, LIST_SCROLL);
    propInfo = (struct PropInfo *) gadget->SpecialInfo;
    num = NumListItems(windList);
    newOffset = (LONG) propInfo->VertPot*(num - NUM_ENTRIES);
    newOffset = (newOffset + 0x7FFF)/0xFFFF;
    DrawWindList(newOffset);
}

/*
 *  Scroll list in direction indicated by gadget
 *  Called by TrackGadget()
 */

void ScrollList(gadgWindow, gadgNum)
struct Window *gadgWindow;
register WORD gadgNum;
{
    if (gadgWindow != window)
	return;
    switch (gadgNum) {
    case LIST_UP:
	if (listOffset > 0)
	    DrawWindList(listOffset - 1);
	break;
    case LIST_DOWN:
	if (listOffset < NumListItems(windList) - NUM_ENTRIES)
	    DrawWindList(listOffset + 1);
	break;
    }
}

/*
 *  De-select all items in list
 */

void DeSelectAll()
{
    register UBYTE *text;
    register WORD i, num;

    num = NumListItems(windList);
    for (i = 0; i < num; i++) {
	text = GetListItem(windList, (LONG) i);
	*text = ' ';
    }
}

/*
 *  Handle gadget down event
 */

void DoGadgetDown(gadget)
struct Gadget *gadget;
{
    register WORD gadgNum;

    if (windList == NULL)
	return;
    gadgNum = GadgetNumber(gadget);
    if (gadgNum == LIST_UP || gadgNum == LIST_DOWN)
	TrackGadget(mainMsgPort, window, gadget, ScrollList);
}

/*
 *  Handle gadget up event
 */

void DoGadgetUp(gadget)
struct Gadget *gadget;
{
    register WORD gadgNum;

    if (windList == NULL)
	return;
    gadgNum = GadgetNumber(gadget);
    switch (gadgNum) {
    case COPY_BUTTON:
	DoCopy();
	break;
    case REMOVE_BUTTON:
	DoRemove();
	break;
    case LIST_SCROLL:
	AdjustToScrollBar();
	break;
    }
}

/*
 *  Handle mouse down event
 */

void DoMouseDown(qualifier, mouseX, mouseY)
UWORD qualifier;
register WORD mouseX, mouseY;
{
    register UBYTE *text;
    register WORD i, item, num;
    WORD minX, minY, maxX, maxY;

    if (windList == NULL)
	return;
    num = NumListItems(windList);
/*
    Determine which entry was selected
*/
    minX = window->BorderLeft + (20 + 1);
    maxX = minX + (LIST_WIDTH - IMAGE_ARROW_WIDTH - 3);
    minY = window->BorderTop + (27 + 1);
    maxY = minY + (NUM_ENTRIES*8 - 1);
    if (mouseX < minX || mouseX >= maxX || mouseY < minY || mouseY >= maxY)
	return;
    item = listOffset + (mouseY - minY)/8;
    if (item >= num)
	return;
/*
    Select item
*/
    text = GetListItem(windList, item);
    if (qualifier & SHIFTKEYS) {
	if (*text == ' ')
	    *text = '*';
	else
	    *text = ' ';
    }
    else {
	DeSelectAll();
	*text = '*';
    }
    DrawWindList(listOffset);
    SetButtons();
/*
    If only one item selected then show info about selected item
*/
    EraseInfo();
    item = -1;
    for (i = 0; i < num; i++) {
	text = GetListItem(windList, (LONG) i);
	if (*text == '*') {
	    if (item != -1)		/* More than one item selected */
		return;
	    item = i;
	}
    }
    if (item != -1)
	ShowInfo(item);
}
