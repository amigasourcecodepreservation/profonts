/* These macros are for extracting an integer of a certain type
 * from the midst of an arbitrary chunk of memory.
 *
 * Due to the offset based Macintosh resource system you have to
 * do this a lot...
 *
 */

#define UC(p) ((unsigned char *)(p))

#ifdef MPU68000

/* this will work on any machine with 68000 style endain ordering */
/* p should be a pointer to char */

#define X_ULONG(p) (*((ULONG *)(p)))
#define X_UWORD(p) (*((UWORD *)(p)))
#define X_UBYTE(p) (*((UBYTE *)(p)))

#define X_LONG(p) (*((LONG *)(p)))
#define X_WORD(p) (*((WORD *)(p)))
#define X_BYTE(p) (*((BYTE *)(p)))

#else

/* this will work on any machine but it's not as fast as the above */


#define X_ULONG(p) (ULONG)(UC(p)[0]<<24|UC(p)[1]<<16|UC(p)[2]<<8|UC(p)[3])
#define X_UWORD(p) (UWORD)(UC(p)[0]<<8|UC(p)[1])
#define X_UBYTE(p) (UBYTE)(UC(p)[0])
#define X_LONG(p)  (LONG)((UC(p)[0]<<24|UC(p)[1]<<16|UC(p)[2]<<8|UC(p)[3])
#define X_WORD(p)  (WORD)(UC(p)[0]<<8|UC(p)[1])
#define X_BYTE(p)  (BYTE)(UC(p)[0])

#endif

/* artificial types... have to fake them */

#define X_UTRIP(p) (ULONG)(UC(p)[0]<<16|UC(p)[1]<<8|UC(p)[2])
#define X_TRIP(p)  (LONG)(UC(p)[0]<<16|UC(p)[1]<<8|UC(p)[2])
