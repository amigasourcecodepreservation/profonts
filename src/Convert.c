/*
 *	Macintosh font conversion program
 */

#include <exec/types.h>
#include <exec/nodes.h>
#include <libraries/diskfont.h>

#include <stdio.h>

#define HUNK_HEADER  0x03F3L
#define HUNK_CODE    0x03E9L
#define HUNK_RELOC32 0x03ECL
#define HUNK_END     0x03F2L

typedef struct {
	WORD  fontType;
	WORD  firstChar;
	WORD  lastChar;
	WORD  widMax;
	WORD  kernMax;
	WORD  nDescent;
	WORD  fRectWid;
	WORD  chHeight;
	WORD  owTLoc;
	WORD  ascent;
	WORD  descent;
	WORD  leading;
	WORD  rowWords;
} FontRec;

UBYTE *bitImage;
WORD  locTable[258];
WORD  owTable[258];

extern BYTE *strchr(), *malloc();

extern WORD  fgetw(FILE *);
extern void fputw(WORD, FILE *);
extern void fputl(LONG, FILE *);

/*
 *	Utility routines
 */

WORD fgetw(file)
FILE *file;
{
	WORD byte;

	byte = fgetc(file);
	return ((WORD) ((byte << 8) | fgetc(file)));
}

void fputw(word, file)
WORD word;
FILE *file;
{
	fputc(word >> 8, file);
	fputc(word & 0xFF, file);
}

void fputl(l, file)
LONG l;
FILE *file;
{
	fwrite((BYTE *) &l, 4, 1, file);
}

/*
 *	Main routine
 */

void main(argc, argv)
int argc;
char *argv[];
{
	register WORD i, c, ow, offset, width;
	WORD entries, size;
	UWORD iStart, iEnd;
	register FILE *file;
	FontRec fontRec;
	ULONG rowBytes, dataSize;
	ULONG numChars, fontName, fontData, fontLoc, fontSpace;
	ULONG fontKern, fontEnd, fontLength;
	ULONG lock;
	char *s, buf[128];
	int padBytes;

	if (argc != 2) {
		printf("Usage: %s fontname\n", argv[0]);
		exit(1);
	}
	if ((s = strchr(argv[1], '.')) == NULL) {
		printf("Error fontname must be of the form:\n");
		printf("\tname.pointsize\n");
		exit(1);
	}
	if ((file = fopen(argv[1], "r")) == NULL) {
		printf("Error can't open file '%s'\n", argv[1]);
		exit(1);
	}
	fread((BYTE *) &fontRec, sizeof(FontRec), 1, file);

	printf("Font Data for %s:\n", argv[1]);
	*s = '\0';								/* remove the suffix */
	printf("\tChars %d to %d\n", fontRec.firstChar, fontRec.lastChar);
	printf("\tFont Rectangle (W, H): (%d, %d)\n", fontRec.fRectWid,
		   fontRec.chHeight);
	printf("\tAscent: %d,  Descent: %d,  Leading: %d\n", fontRec.ascent,
		   fontRec.descent, fontRec.leading);

	rowBytes = fontRec.rowWords*2;
	dataSize = rowBytes*fontRec.chHeight;
	if ((bitImage = malloc(dataSize)) == NULL) {
		printf("Error, no memory for bit image, need: %d\n", dataSize);
		exit(1);
	}

	fread(bitImage, dataSize, 1, file);

	for(i = fontRec.firstChar; i <= fontRec.lastChar + 2; i++)
		locTable[i] = fgetw(file);

	for(i = fontRec.firstChar; i <= fontRec.lastChar + 2; i++)
		owTable[i] = fgetw(file);

	fclose(file);

	sprintf(buf, "fonts:%s", argv[1]);
	if ((lock = CreateDir(buf)) != NULL)
		UnLock(lock);

	sprintf(buf, "fonts:%s/%d",argv[1], fontRec.chHeight);
	if ((file = fopen(buf, "w")) == NULL) {
		printf("Error, can't open '%s' for write\n", buf);
		exit(1);
	}
/*
	Compute offsets into the code hunk
*/
	numChars   = fontRec.lastChar - fontRec.firstChar + 2;
	fontName   = 0x1A;
	fontData   = 0x6E;
	fontLoc	= fontData  + dataSize;
	fontSpace  = fontLoc   + (numChars << 2);
	fontKern   = fontSpace + (numChars << 1);
	fontEnd	= fontKern  + (numChars << 1);
/*
	Long word align fontEnd
*/
	for (padBytes = 0; fontEnd & 3; padBytes++, fontEnd++) ;
	fontLength = fontEnd - 0x3A;		/* the font: label */

	fputl(HUNK_HEADER, file);
	fputl(   0L, file);						/* no hunk names */
	fputl(   1L, file);						/* Size of hunk table */
	fputl(   0L, file);						/* First Hunk */
	fputl(   0L, file);						/* Last Hunk */
	fputl((LONG) fontEnd >> 2, file);		/* Size of Hunk 0 */

	fputl(HUNK_CODE, file);
	fputl((LONG) fontEnd >> 2, file);		/* Size of this hunk */
/*
	Now here comes the data for the hunk
	This first part is in case someone tries to run this
*/
	fputw(0x7000, file);			/* MOVEQ.L #0,D0 */
	fputw(0x4E75, file);			/* RTS */

	fputl(   0L, file);				/* ln_Succ */
	fputl(   0L, file);				/* ln_Pred */
	fputc(NT_FONT, file);			/* ln_Type */
	fputc(   0, file);				/* ln_Pri  */
	fputl(0x1AL, file);				/* ln_Name -- will be relocated */
	fputw(DFH_ID, file);			/* FileID */
	fputw(   1, file);				/* Revision */
	fputl(   0L, file);				/* Segment */

/* fontName: */

	for (i = 0; i < MAXFONTNAME; i++)
		fputc(0, file);

/* font: */

	fputl(   0L, file);				/* ln_Succ */
	fputl(   0L, file);				/* ln_Pred */
	fputc(NT_FONT, file);			/* ln_Type */
	fputc(   0, file);				/* ln_Pri  */
	fputl(0x1AL, file);				/* ln_Name -- will be relocated */
	fputl(   0L, file);				/* mn_ReplyPort */
	fputw((WORD) fontLength, file);	/* nm_Length */
	fputw(fontRec.chHeight, file);	/* tf_YSize */
	fputc(   0, file);				/* tf_Style */
	fputc(0x62, file);				/* tf_Flags */
	fputw(fontRec.chHeight, file);	/* tf_XSize */
	fputw(fontRec.ascent, file);	/* ft_BaseLine */
	fputw(   1, file);				/* tf_BoldSmear */
	fputw(   0, file);				/* tf_Accessors */
	fputc(fontRec.firstChar, file);	/* tf_LoChar */
	fputc(fontRec.lastChar, file);	/* tf_HiChar */
	fputl((LONG) fontData, file);	/* tf_CharData */
	fputw((WORD) rowBytes, file);	/* tf_Modulo */
	fputl((LONG) fontLoc, file);	/* tf_CharLoc */
	fputl((LONG) fontSpace, file);	/* tf_CharSpace */
	fputl((LONG) fontKern, file);	/* tf_Charkern */

/* fontData: */

	fwrite(bitImage, dataSize, 1, file);

/* fontLoc: */

/*
	Note that there is an extra character in every font on both
	that Amiga and the Mac which is used when a character not in
	the font is requested
*/
	for (i = fontRec.firstChar; i <= fontRec.lastChar + 1; i++) {
		c = (owTable[i] == -1) ? fontRec.lastChar + 1 : i;
		iStart = locTable[c];
		iEnd   = locTable[c + 1];
		fputw((WORD) iStart, file);
		fputw((WORD) (iEnd - iStart), file);
	}

/* fontSpace: */

	for (i = fontRec.firstChar; i <= fontRec.lastChar + 1; i++) {
		if ((ow = owTable[i]) == -1)
			ow = owTable[fontRec.lastChar + 1];
		offset = (ow >> 8) & 0xFF;
		width = ow & 0xFF;
		fputw((WORD) (width - offset - fontRec.kernMax), file);
	}

/* fontKern: */

	for (i = fontRec.firstChar; i <= fontRec.lastChar + 1; i++) {
		if ((ow = owTable[i]) == -1)
			ow = owTable[fontRec.lastChar + 1];
		offset = (ow >> 8) & 0xFF;
		fputw((WORD) (offset + fontRec.kernMax), file);
		if (offset + fontRec.kernMax < 0)
			printf("Warning - negative kern for char %c ($%02x)\n", i, i);
	}

/* fontEnd: */

	for (i = 0; i < padBytes; i++)
		fputc(0, file);
	fputl(HUNK_RELOC32, file);
	fputl(   6L, file);			/* of relocates number in this hunk */
	fputl(   0L, file);			/* hunk 0 */
	fputl(0x0EL, file);			/* relocate reference to fontName: 1st */
	fputl(0x44L, file);			/* relocate reference to fontName: 2nd */
	fputl(0x5CL, file);			/* relocate reference to fontData:  */
	fputl(0x62L, file);			/* relocate reference to fontLoc:   */
	fputl(0x66L, file);			/* relocate reference to fontSpace: */
	fputl(0x6AL, file);			/* relocate reference to fontKern:  */
	fputl(   0L, file);			/* zero to mark end */
	fputl(HUNK_END, file);

	fclose(file);

	sprintf(buf, "fonts:%s.font", argv[1]);
	if ((file = fopen(buf,"r+")) == NULL) {
		if ((file = fopen(buf, "w+")) == NULL) {
			printf("Error, can't create font header\n");
			exit(1);
		}
		fputw(0x0F00, file);	/* font header */
		fputw(0x0000, file);	/* number of font items */
	}

	fseek(file, 2, 0);
	fread((BYTE *) &entries, 2, 1, file);

	for (i = 0; i < entries; i++) {
		fseek(file, 4 + (260*i) + 256, 0);
		fread((BYTE *) &size, 2, 1, file);		/* read in ysize */
		if (size == fontRec.chHeight) {
			printf("Amiga font of this size already exists");
			printf("... overwritten\n");
			exit(1);
		}
	}

	fseek(file, 2, 0);
	fputw(++entries, file);
	fseek(file, 0, 2);
	sprintf(buf, "%s/%d", argv[1], fontRec.chHeight);
	fwrite(buf, strlen(buf), 1, file);
	for (i = strlen(buf); i < 256; i++)
		fputc(0, file);
	fputw(fontRec.chHeight, file);
	fputc(   0, file);
	fputc(0x62, file);
	fclose(file);
}
