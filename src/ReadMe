			Macintosh font conversion package
			---------------------------------

First off, I'd like to thank John O'Neill for writing the Mac suitcase
unpacker and for generally letting me in on what's going on with Mac
fonts.   Thanks John!

Conversion of fonts is a 3 step process:


1) Transfer a Mac font suitcase file to the Amiga using the method of 
   your choice.  The closer you can get to sending the exact unmodified
   binary the happier you'll be.  The fonts that I've converted (yes
   I have test this beast :-) ) we're shipped from the Mac to our
   VAX 750 using FreeTerm and then from the VAX to the Amiga using
   the kermit binary transfer facility in vt100 v2.6.  This works for me.

2) Unpack the suitcase you have just downloaded to the Amiga using the
   'unpack' program.  The -x option forces the program to ignore the
   first 128 bytes, this option is included because the transfer method
   described above caused a 128 byte .info header to be prepended to the
   suitcase data.  This was originally always done as part of the unpacking
   but I turned it into an option just in case...  The output of unpack
   is a zillion files, one for each font at each size in the suitcase.  
   The files are all of the form  Font_Name.pointsize.

3) Convert the raw Mac font resources into Amiga fonts using the 'convert'
   program.  Convert only converts one font at a time... so in practices
   you use you're favourite shell and something like a 

   	foreach i ( *.* ) "convert $i"

   loop.  Convert creates the required .font files, makes the directorys
   required and creates the font executable (did you know that Amiga
   fonts are executable?).  Note:  Mac fonts are named by their point
   size, e.g.  Chicago 12 is a 12 point font.  Amiga fonts are named
   by their pixel height, e.g. Topaz 11 is 11 pixels high.  This means
   that the font will convert into a different size number, e.g.
   Beverly Hills 72 becomes Beverly Hills 84.  Further sometimes there
   are serveral point sizes that end up being the same number of pixels
   high (due to positition of the baseline, leading, etc.) this means
   that two Mac fonts might map onto the same Amiga font size.  Currently
   the convert program just overwrites the old font in that case.

   I recommend you reassign fonts: to somewhere convenient before
   performing the conversion as there will likely be many new fonts
   added to the system and there are only a few Amiga programs that deal
   with many fonts gracefully.  (This is only a problem when running
   software that tries to let you choose your font from a menu or
   a requestor, it isn't a problem with the way the OS deal with fonts).


Permission is granted to freely redistribute any or all of the font
conversion package sources, documents & executeable provided that there is
no charge for such redistribution above a normal disk copying fee or
download/connect time charge.  e.g. it's OK to put these on any BBS,
online service, or disk library; it's not OK to market them for profit.
Any use other than indicated above requires written permission from
both authors.

Please be aware when doing font conversions that many Mac fonts are
copyrighted by various individuals/companies and that doing a conversion
of such a font may be in violation of the laws of your country.  There
are many nice public domain Mac fonts and it will please me no end if
the use of this program is completely limited to those fonts.

	-Rico
